NB-LIB Development Style-Guide
==============================

Naming Convention for Include-Guards in Headers
-----------------------------------------------

For the files in the top-level directory of the project, the naming would follow the format of ``NBLIB_HEADERFILENAME_H`` or ``NBLIB_HEADERFILENAME_HPP``.

For the files within a subdirectory in the project, the format would be ``NBLIB_SUBDIRNAME_HEADERFILENAME_H`` to avoid potential name clashes.

Example for the file ``nblib/util/user.h``:

.. code:: cpp

   /*
    * Copyright text
    */
   /*! \inpublicapi \file
    * \brief
    * Doxygen doc strings followed by author(s)
    *
    * \author NBLIB Developer <read_the_docs@reduce_review_time.org>
    */
    #ifndef NBLIB_UTIL_USER_H
    #define NBLIB_UTIL_USER_H

    // Contents of the header file
    #endif // NBLIB_UTIL_USER_H

Asserts
-------

NBLIB asserts should have the following format

.. code:: cpp

    std::string message = "Meaningful error message";
    assert((!condition && message.c_str()));

where condition is a bool or function returning a bool and the message pre-aggregates relevant information about what triggered the assert.

Asserts should be used only to check class/method invariants in situations where user input is extremely unlikely to be the error source.

Use of StrongType in function signatures
----------------------------------------

NB-LIB provides a StrongType functionality.
This is useful to prevent incorrect argument ordering at the call site via compile time checking.
In the public NB-LIB API it is preferred to use a dedicated StrongType in function signatures.
Using type int in function signatures is allowed in reasonable cases such as passing one index.
